﻿using GDM.FeedProducts;
using GDM.FeedProducts.Core;
using GDM.FeedProducts.Core.PipelineProcessor;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

using IHost host = Host.CreateDefaultBuilder(args)
    .ConfigureServices(services =>
    {
        services.AddScoped<ILogger, ConsoleLogger>();
        services.AddFeedProducts();
    })
    .Build();

RunProcess(host.Services, args);

await host.RunAsync();



static void RunProcess(IServiceProvider hostProvider, string[] args)
{
    if (args.Length != 2)
    {
        Console.WriteLine("The application needs two arguments, filename and operator" +
            "e.g. feedproducts c:/temp/provider-file.yaml providerName");
    }

    // FIXME: add better validation for arguments.

    using IServiceScope serviceScope = hostProvider.CreateScope();
    IServiceProvider provider = serviceScope.ServiceProvider;
    var pipeline = provider.GetRequiredService<Pipeline>();
    pipeline.ProcessFile(new Context(filePath: args[0], provider: args[1]));
}