﻿using GDM.FeedProducts.Core.Parser;
using GDM.FeedProducts.Core.PipelineProcessor;
using Microsoft.Extensions.DependencyInjection;

namespace GDM.FeedProducts.Core
{
    public static class ServiceCollectionExtensions
    {
        public static void AddFeedProducts(this IServiceCollection services)
        {
            services.AddScoped<IFileParser, CapterraFileParser>();
            services.AddScoped<Pipeline>();
            services.Chain<IProcessorHandler>()
                   .Add<ValidationHandler>()
                   .Add<ParsingHandler>()
                   .Configure();
        }
    }
}