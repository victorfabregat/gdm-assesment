﻿namespace GDM.FeedProducts.Core
{
    public interface ILogger
    {
        void Info(string message);
        public void Warn(string message);
    }
}
