﻿namespace GDM.FeedProducts.Core.Parser
{
    /// <summary>
    ///  Class to model the YAML content of Capterra provider
    /// </summary>
    public class CapterraYaml
    {
        public string? Name { get; set; }

        public string? Twitter { get; set; }

        public string? Tags { get; set; }
    }
}
