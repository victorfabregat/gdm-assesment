﻿using GDM.FeedProducts.Core.Domain;
using YamlDotNet.Serialization.NamingConventions;
using YamlDotNet.Serialization;

namespace GDM.FeedProducts.Core.Parser
{
    /// <summary>
    /// Parser for files provided by Capterra in Yaml format
    /// </summary>
    public partial class CapterraFileParser : IFileParser
    {
        public Provider Provider => Provider.Create("Capterra");

        public IReadOnlyList<Product> Parse(string fileContent)
        {
            var deserializer = new DeserializerBuilder()
                                    .WithNamingConvention(UnderscoredNamingConvention.Instance)
                                    .Build();

            var rawProducts = deserializer.Deserialize<List<CapterraYaml>>(fileContent);

            // TODO: This can have issues with performance, and also is a bit complex code.
            return rawProducts
                    .Select(p => new Product(
                                        p.Name ?? string.Empty, 
                                        p.Twitter ?? string.Empty, 
                                        Provider, 
                                        p.Tags?.Split(",").Select(t => new Tag(t))))
                    .ToList()
                    .AsReadOnly();
        }
    }
}
