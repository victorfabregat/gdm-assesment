﻿using GDM.FeedProducts.Core.Domain;

namespace GDM.FeedProducts.Core.Parser
{
    public interface IFileParser
    {
        Provider Provider { get; }

        IReadOnlyList<Product> Parse(string fileContent);
    }
}
