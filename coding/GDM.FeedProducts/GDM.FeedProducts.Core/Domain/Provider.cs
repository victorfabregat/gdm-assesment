﻿namespace GDM.FeedProducts.Core.Domain
{
    public class Provider : ValueObject
    {
        public string Name { get; }

        public Provider(string name)
        {
            Name = name;
        }

        public static Provider Create(string name) => new Provider(name);

        protected override IEnumerable<object> GetEqualityComponents()
        {
            yield return Name;
        }
    }
}
