﻿using System.Text;

namespace GDM.FeedProducts.Core.Domain
{
    public class Product
    {
        // Note: It is not required to have an ID, I add it just in case, review
        public Guid Id { get; }

        // TODO: This can be also a value object
        public string Name { get; }

        // TODO: This can be also a value object
        public string Twitter { get; }

        public Provider Provider { get; }

        public ICollection<Tag> Tags { get; }

        public Product(string name, string twitter, Provider provider, IEnumerable<Tag>? tags)
        {
            Id = Guid.NewGuid();
            Name = name;
            Twitter = twitter;
            Provider = provider;
            Tags = tags?.ToHashSet() ?? new HashSet<Tag>();
        }

        internal string ToPrintableString()
        {
            var sb = new StringBuilder();
            sb.AppendLine(Name);
            sb.AppendLine(Twitter);
            sb.AppendLine(Tags.Select(t => t.Name).Aggregate((c, n) => c + ", " + n));
            return sb.ToString();
        }
    }
}
