﻿namespace GDM.FeedProducts.Core.Domain
{
    public class Tag : ValueObject
    {
        public string Name { get; }

        public Tag(string name)
        {
            Name = name;
        }

        protected override IEnumerable<object> GetEqualityComponents()
        {
            yield return Name;
        }
    }
}
