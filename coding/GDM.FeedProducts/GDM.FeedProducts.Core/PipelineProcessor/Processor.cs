﻿namespace GDM.FeedProducts.Core.PipelineProcessor
{
    /// <summary>
    /// Entry point for the pipeline processor
    /// </summary>
    public class Pipeline
    {
        private IProcessorHandler _handler;
        public Pipeline(IProcessorHandler handler)
        {
            _handler = handler;
        }

        public void ProcessFile(Context args)
        {
            _handler.Handle(args);
        }
    }
}
