﻿using System.IO;
using System.Text;
using GDM.FeedProducts.Core.Domain;
using GDM.FeedProducts.Core.Parser;

namespace GDM.FeedProducts.Core.PipelineProcessor
{
    public class ParsingHandler : IProcessorHandler
    {
        public IProcessorHandler Next { get; }

        private readonly IEnumerable<IFileParser> _files;

        private readonly ILogger _logger;

        public ParsingHandler(
            IProcessorHandler next,
            IEnumerable<IFileParser> parsers,
            ILogger logger)
        {
            Next = next;
            _files = parsers;
            _logger = logger;
        }

        public void Handle(Context args)
        {
            var currentProvider = Provider.Create(args.Provider);

            var providerParser = _files.FirstOrDefault(f => f.Provider == currentProvider);
            if (providerParser == null)
            {
                _logger.Warn($"Parser not found for provider {args.Provider}");
                return;
            }
            string content = string.Empty;

            using var fs = new FileStream(args.FilePath, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
            using var sr = new StreamReader(fs, Encoding.Default);

            content = sr.ReadToEnd();

            var products = providerParser.Parse(content);

            _logger.Info("Products parsed correclty");
            foreach (var product in products)
            {
                _logger.Info(product.ToPrintableString());
            }

            args.AddProducts(products);


            Next?.Handle(args);
        }
    }
}
