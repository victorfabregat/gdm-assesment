﻿namespace GDM.FeedProducts.Core.PipelineProcessor
{
    public interface IProcessorHandler
    {
        IProcessorHandler Next { get; }

        void Handle(Context args);
    }
}
