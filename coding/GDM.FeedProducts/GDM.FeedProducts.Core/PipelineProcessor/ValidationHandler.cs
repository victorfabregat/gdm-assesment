﻿namespace GDM.FeedProducts.Core.PipelineProcessor
{
    /// <summary>
    /// Handler to validate the input, only will pass if the file is correct
    /// </summary>
    public class ValidationHandler : IProcessorHandler
    {
        public IProcessorHandler Next { get; }

        private ILogger _logger;

        public ValidationHandler(IProcessorHandler next, ILogger logger)
        {
            Next = next;
            _logger=logger;
        }

        public void Handle(Context args)
        {
            _logger.Warn($"running a fake validation, file: {args.FilePath} provider: {args.Provider}");

            // FIXME: This is only called when the validations is sucessful
            Next?.Handle(args);
        }
    }
}
