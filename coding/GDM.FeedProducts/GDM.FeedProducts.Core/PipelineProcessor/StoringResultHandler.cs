﻿namespace GDM.FeedProducts.Core.PipelineProcessor
{
    public class StoringResultHandler : IProcessorHandler
    {
        public IProcessorHandler Next { get; }

        private ILogger _logger;

        public StoringResultHandler(IProcessorHandler next, ILogger logger)
        {
            Next = next;
            _logger=logger;
        }

        public void Handle(Context args)
        {
            // FIXME: We need to store the args.Products here.
            // to allow changing the storage implementation we can use an abstraction, as we did with the logger.
        }
    }
}
