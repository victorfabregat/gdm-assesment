﻿using GDM.FeedProducts.Core.Domain;

namespace GDM.FeedProducts.Core.PipelineProcessor
{
    /// <summary>
    /// Arguments to pass information through the pipeline.
    /// </summary>
    public class Context
    {
        public string FilePath { get; }
        public string Provider { get; }

        public IReadOnlyList<Product> Products { get; private set; } 

        public Context(string filePath, string provider)
        {
            FilePath = filePath;
            Provider = provider;
        }

        public void AddProducts(IReadOnlyList<Product> products)
        {
            Products = products;
        }
    }
}
