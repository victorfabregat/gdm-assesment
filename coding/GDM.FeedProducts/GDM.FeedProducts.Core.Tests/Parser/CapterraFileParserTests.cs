﻿using GDM.FeedProducts.Core.Domain;
using GDM.FeedProducts.Core.Parser;
using Xunit;

namespace GDM.FeedProducts.Core.Tests.Parser
{
    public class CapterraFileParserTests
    {
        [Fact]
        public void ValidFileContentShouldReturnListOfObjects()
        {
            var sut = new CapterraFileParser();

            var results = sut.Parse(@"
-
  tags: ""Bugs & Issue Tracking,Development Tools""
  name: ""GitHub""
  twitter: ""github""
");
            Assert.Equal(2, results.Count);
        }
    }
}