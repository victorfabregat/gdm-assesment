## Approach

The process needs to execute three steps, validation, processing and storing. We need to execute those steps in that order, and only if the previous step was successful.

The steps are the same for all providers, but the implementation depends on the provider file schema.

In order to concatenate steps I used a [Chain of responsability](https://refactoring.guru/design-patterns/chain-of-responsibility/csharp/example) in combination with Dependency injection configuration.

This will give us flexibility in the future to add more steps.

Each step depends on abstraction, so we can add more implementations for providers.

This is all in theory I found in real projects that this can be a bit messy if the product supports multiple providers.

## Notes conventions

To give more context about some decisions in the code I used three kinds of comments:

_NOTE:_ this is to give some information about the implementation. When I get some code from the internet I usually paste the link as a reference, in the future you may want to see the original implementation.

_TODO:_ This is something that can be improved, and some information on how I think that can be improved.

_FIXME:_ This is something required to complete the test, but I didn't manage to get it done, so I write a brief explanation of how I think that can be implemented.

## How to run the project
I used Visual Studio, and for me, the best option is to open from there. If you want to execute the project running the following command from the executable location will work

```
.\FeedProducts.exe c:\feed-products\capterra.yaml Capterra
```

## Things that I would improve

### Tests
I use the test to write the YAML parsing code, but there is only a test, I would like to add more tests to validate the handlers and also to validate the dependencies registration code because there is some logic there.

### Performance
I didn't take into consideration the performance, so there are a few pieces of the code iterating over the results multiple times, this can be a problem in the future.

### Over abstraction
I use a few interfaces and abstractions to make it flexible to add more implementations, but sometimes I feel that this over-abstraction adds more complexity to the code.
