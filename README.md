## Software Engineer Home Assignment

This assignment is split in two part: coding and database. Please, proceed to a corresponding folder to read detailed instructions. 

We estimated for it to take approximately 2-3 hours. Once completed, share your results in a public git repository (Github, Gitlab, Bitbucket).

Good luck!