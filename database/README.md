# SQL Test Assignment

Attached is a mysqldump of a database to be used during the test.

Below are the questions for this test. Please enter a full, complete, working SQL statement under each question. We do not want the answer to the question. We want the SQL command to derive the answer. We will copy/paste these commands to test the validity of the answer.

**Example:**

_Q. Select all users_

- Please return at least first_name and last_name

SELECT first_name, last_name FROM users;


------

**— Test Starts Here —**

1. Select users whose id is either 3,2 or 4
- Please return at least: all user fields
-- For simplicity I use IN
SELECT *
FROM users u 
WHERE id in (2,3,4)

-- But always I thought that is not the best option in terms of performance,
-- so this is also an option:

SELECT *
FROM users u
where id = 2
or id = 3
or id = 4

-- I compared both with the execution plan, and I didn't see much difference. Even though I'm not an expert and is my first time looking at MySql execution plans

2. Count how many basic and premium listings each active user has
- Please return at least: first_name, last_name, basic, premium
- 
/* I'm not a fan about double joins, but it works... 
 * propably with a CTE will be better, but for this case I think that the double join is good enough
*/
SELECT u.first_name , u.last_name , COUNT(lbasic.id) as basic, COUNT(lprem.id) as premium
FROM users u 
LEFT JOIN listings lbasic ON lbasic.user_id = u.id 
	AND lbasic.status = 2 -- basic
LEFT JOIN listings lprem ON lprem.user_id = u.id 
	AND lprem.status = 3 -- premium
WHERE u.status = 2 -- enabled
GROUP BY u.first_name , u.last_name


3. Show the same count as before but only if they have at least ONE premium listing
- Please return at least: first_name, last_name, basic, premium
SELECT u.first_name , u.last_name , COUNT(lbasic.id) as basic, COUNT(lprem.id) as premium
FROM users u 
LEFT JOIN listings lbasic ON lbasic.user_id = u.id 
	AND lbasic.status = 2 -- basic
LEFT JOIN listings lprem ON lprem.user_id = u.id 
	AND lprem.status = 3 -- premium
WHERE u.status = 2 -- enabled
GROUP BY u.first_name , u.last_name
HAVING COUNT(lprem.id) > 0


4. How much revenue has each active vendor made in 2013
- Please return at least: first_name, last_name, currency, revenue
/* assuming that users are vendors.
reference for year function: https://stackoverflow.com/a/17031473/1322417 */
SELECT u.first_name, u.last_name, c.currency, SUM(c.price) as revenue
FROM users u 
INNER JOIN listings l ON l.user_id = u.id
INNER JOIN clicks c ON c.listing_id = l.id 
WHERE u.status = 2 -- enabled
AND YEAR(c.created) = 2013
GROUP BY u.first_name , u.last_name, c.currency 

5. Insert a new click for listing id 3, at $4.00
- Find out the id of this new click. Please return at least: id
INSERT INTO clicks
(listing_id, price, currency, created)
VALUES(3, 4.00, 'USD', now());

SELECT LAST_INSERT_ID() as inserted_click_id;

6. Show listings that have not received a click in 2013
- Please return at least: listing_name
SELECT DISTINCT l.name 
FROM listings l 
WHERE l.id NOT IN (
	SELECT distinct listing_id
	FROM clicks c 
	WHERE YEAR(c.created) = 2013
) -- clicks on 2013
ORDER BY l.name 

7. For each year show number of listings clicked and number of vendors who owned these listings
- Please return at least: date, total_listings_clicked, total_vendors_affected
-- assuming that users are vendors.
SELECT YEAR(c.created), 
		COUNT(l.id) as total_listings_clicked, 
		COUNT(l.user_id) as total_vendors_affected
FROM clicks c
INNER JOIN listings l ON l.id = c.listing_id 
group by YEAR(c.created)


8. Return a comma separated string of listing names for all active vendors
- Please return at least: first_name, last_name, listing_names

/*First time using this group_concat function, I've never worked before with MySql,
 * I get the hint from here https://stackoverflow.com/a/276949/1322417
 * and here how yo use it: https://dev.mysql.com/doc/refman/5.6/en/aggregate-functions.html#function_group-concat*/
SELECT first_name, last_name, GROUP_CONCAT(l.name) as listing_names
FROM users u 
INNER JOIN listings l ON l.user_id = u.id 
group by first_name, last_name 

